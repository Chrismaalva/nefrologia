module.exports = {
  siteMetadata: {
    title: `Nefrología`,
    description: `Sistemas Heredados`,
    author: `@ChristianMartinez`,
  },
  plugins: [
    "gatsby-plugin-svgr",
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-plugin-anchor-links",
      options: {
        offset: -100,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/static/images`,
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        includePaths: ["src", "src/styles", "src/styles/config"],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Allure - Desarrollos inmobiliarios`,
        short_name: `Allure`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#000000`,
        display: `minimal-ui`,
      },
    },
  ],
}
