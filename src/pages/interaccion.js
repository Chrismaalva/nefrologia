import React from "react"
import { SEO, Layout, Interaction } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA
function Interaccion() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Interaction />
    </Layout>
  )
}

export default Interaccion
