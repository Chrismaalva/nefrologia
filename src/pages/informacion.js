import React from "react"
import { SEO, Layout, Information } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA
function Informacion() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Information />
    </Layout>
  )
}

export default Informacion
