import React from "react"
import { SEO, Layout, About } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA

export default function Home() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <About />
    </Layout>
  )
}
