import React from "react"
import { SEO, Layout, Help } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA
function Ayuda() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Help />
    </Layout>
  )
}

export default Ayuda
