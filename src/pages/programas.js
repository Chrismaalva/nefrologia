import React from "react"
import { SEO, Layout, Programs } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"
const { layoutProps } = NEFROLOGIA_DATA

function Programas() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Programs />
    </Layout>
  )
}

export default Programas
