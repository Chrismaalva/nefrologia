import React from "react"
import { SEO, Layout } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

import "../styles/pages/cuestionario.scss"

const { layoutProps } = NEFROLOGIA_DATA

function Cuestionario() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <div className="Cuestionario__Container">
        <form id="form" action="" method="post">
          <div class="form-item-container">
            <label htmlFor="nombre" for="nombre" class="form-item-label">
              Nombre
            </label>
            <input
              type="text"
              required
              placeholder="Ingrese su nombre"
              class="form-item-input"
              name="first_name"
              id="nombre"
            />
          </div>

          <div class="form-item-container">
            <label htmlFor="apellido" for="apellido" class="form-item-label">
              Apellido
            </label>
            <input
              type="text"
              required
              placeholder="Ingrese su apellido"
              class="form-item-input"
              name="last_name"
              id="apellido"
            />
          </div>

          <div class="form-item-container">
            <label
              htmlFor="correoElectronico"
              for="correoElectronico"
              class="form-item-label"
            >
              Correo Electrónico
            </label>
            <input
              type="text"
              required
              placeholder="Ingrese su correo electronico"
              class="form-item-input"
              name="email"
              id="correoElectronico"
            />
          </div>

          <fieldset>
            <legend>¿Siente trabajo al orinar?</legend>
            <div class="form-item-radio">
              <input
                class="sienteTrabajoAlOrinar"
                type="radio"
                name="problemas_orinar"
                value="Si"
                id="trabajoOrinar1"
              />
              <label htmlFor="trabajoOrinar1" for="trabajoOrinar1">
                Si
              </label>
            </div>
            <div class="form-item-radio">
              <input
                class="sienteTrabajoAlOrinar"
                type="radio"
                name="problemas_orinar"
                value="No"
                id="trabajoOrinar2"
              />
              <label htmlFor="trabajoOrinar2" for="trabajoOrinar2">
                No
              </label>
            </div>
          </fieldset>

          <fieldset>
            <legend>¿Suele tener la presión alta?</legend>
            <div class="form-item-radio">
              <input
                class="presionAlta"
                type="radio"
                name="presion_alta"
                value="Si"
                id="presionAlta1"
              />
              <label htmlFor="presionAlta1" for="presionAlta1">
                Si
              </label>
            </div>
            <div class="form-item-radio">
              <input
                class="presionAlta"
                type="radio"
                name="presion_alta"
                value="No"
                id="presionAlta2"
              />
              <label htmlFor="presionAlta2" for="presionAlta2">
                No
              </label>
            </div>
          </fieldset>

          <fieldset>
            <legend>¿Su orina es espumosa?</legend>
            <div class="form-item-radio">
              <input
                class="orinaEspumosa"
                type="radio"
                name="orina_espumosa"
                value="Si"
                id="orinaEspumosa1"
              />
              <label htmlFor="orinaEspumosa1" for="orinaEspumosa1">
                Si
              </label>
            </div>
            <div class="form-item-radio">
              <input
                class="orinaEspumosa"
                type="radio"
                name="orina_espumosa"
                value="No"
                id="orinaEspumosa2"
              />
              <label htmlFor="orinaEspumosa2" for="orinaEspumosa2">
                No
              </label>
            </div>
          </fieldset>

          <fieldset>
            <legend>
              ¿Presenta hinchazón en las piernas, pies, tobillos o cualquier
              otra parte del cuerpo?
            </legend>
            <div class="form-item-radio">
              <input
                class="hinchazon"
                type="radio"
                name="hinchazon"
                value="Si"
                id="hinchazon1"
              />
              <label htmlFor="hinchazon1" for="hinchazon1">
                Si
              </label>
            </div>
            <div class="form-item-radio">
              <input
                class="hinchazon"
                type="radio"
                name="hinchazon"
                value="No"
                id="hinchazon2"
              />
              <label htmlFor="hinchazon2" for="hinchazon2">
                No
              </label>
            </div>
          </fieldset>

          <fieldset>
            <legend>¿Nota un dolor punzante en la espalda?</legend>
            <div class="form-item-radio">
              <input
                class="espalda"
                type="radio"
                name="dolor_punzante"
                value="Si"
                id="dolorPunzante1"
              />
              <label htmlFor="dolorPunzante1" for="dolorPunzante1">
                Si
              </label>
            </div>
            <div class="form-item-radio">
              <input
                class="espalda"
                type="radio"
                name="dolor_punzante"
                value="No"
                id="dolorPunzante2"
              />
              <label htmlFor="dolorPunzante2" for="dolorPunzante2">
                No
              </label>
            </div>
          </fieldset>

          <fieldset>
            <legend>Marque todos los síntomas que apliquen</legend>
            <div class="form-item-checkbox-flex">
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Inapetencia"
                  id="sintoma1"
                />
                <label htmlFor="sintoma1" for="sintoma1">
                  Inapetencia
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Sensación de malestar general"
                  id="sintoma2"
                />
                <label htmlFor="sintoma2" for="sintoma2">
                  Sensación de malestar general
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Fatiga"
                  id="sintoma3"
                />
                <label htmlFor="sintoma3" for="sintoma3">
                  Fatiga
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Dolores de cabeza"
                  id="sintoma4"
                />
                <label htmlFor="sintoma4" for="sintoma4">
                  Dolores de cabeza
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Náuseas"
                  id="sintoma5"
                />
                <label htmlFor="sintoma5" for="sintoma5">
                  Náuseas
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Perdida de peso"
                  id="sintoma6"
                />
                <label htmlFor="sintoma6" for="sintoma6">
                  Perdida de peso
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Piel anormalmente oscura o clara"
                  id="sintoma7"
                />
                <label htmlFor="sintoma7" for="sintoma7">
                  Piel anormalmente oscura o clara
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Dolor de huesos"
                  id="sintoma8"
                />
                <label htmlFor="sintoma8" for="sintoma8">
                  Dolor de huesos
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Somnolencia o problemas para concentrarse o pensar"
                  id="sintoma9"
                />
                <label htmlFor="sintoma9" for="sintoma9">
                  Somnolencia o problemas para concentrarse o pensar
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Entumecimiento o hinchazón en las manos y los pies"
                  id="sintoma10"
                />
                <label htmlFor="sintoma10" for="sintoma10">
                  Entumecimiento o hinchazón en las manos y los pies
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Sed excesiva"
                  id="sintoma11"
                />
                <label htmlFor="sintoma11" for="sintoma11">
                  Sed excesiva
                </label>
              </div>
              <div class="form-item-checkbox">
                <input
                  class="sintomas"
                  type="checkbox"
                  name="sintomas[]"
                  value="Hipo frecuente"
                  id="sintoma12"
                />
                <label htmlFor="sintoma12" for="sintoma12">
                  Hipo frecuente
                </label>
              </div>
            </div>
          </fieldset>
          <div class="form-item-container">
            <label
              htmlFor="describirSintomas"
              for="describirSintomas"
              class="form-item-label"
            >
              Describa sus síntomas
            </label>
            <textarea
              name="descripcion"
              id="describirSintomas"
              cols="30"
              rows="10"
            ></textarea>
          </div>
          <input
            id="submitButton"
            class="form-button"
            name="submit"
            type="submit"
            value="Enviar"
          />
        </form>
      </div>
    </Layout>
  )
}

export default Cuestionario
