import React from "react"
import { SEO, Layout, Faq } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA

function PreguntasFrecuentes() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Faq />
    </Layout>
  )
}

export default PreguntasFrecuentes
