import React from "react"
import { SEO, Layout, Doctors } from "../components"
import { NEFROLOGIA_DATA } from "../data/nefrologia-data"

const { layoutProps } = NEFROLOGIA_DATA

function Doctores() {
  return (
    <Layout {...layoutProps}>
      <SEO title="Home" />
      <Doctors />
    </Layout>
  )
}

export default Doctores
