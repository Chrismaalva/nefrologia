export const NEFROLOGIA_DATA = {
  layoutProps: {
    headerLinks: [
      { text: "Inicio", url: "/" },
      { text: "Información", url: "/informacion" },
      { text: "Doctores", url: "/doctores" },
      { text: "Programas", url: "/programas" },
      { text: "Interacción", url: "/interaccion" },
      { text: "Ayuda", url: "/ayuda" },
    ],
  },
}
