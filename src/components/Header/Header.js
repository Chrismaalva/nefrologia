import React from "react"
import { Link } from "gatsby"
import { AnchorLink } from "gatsby-plugin-anchor-links"

import "./header.scss"

const NavLinks = ({ links }) => (
  <>
    {links.map((link, idx) => (
      <li className="Header__navItem" key={`nav-${idx}`}>
        <Link
          className="Header__navLink"
          to={link.url}
          dangerouslySetInnerHTML={{ __html: link.text }}
        />
      </li>
    ))}
  </>
)

const AnchorLinks = ({ links }) => (
  <>
    {links.map((link, idx) => (
      <li className="Header__navItem" key={`nav-${idx}`}>
        <AnchorLink className="Header__navLink Header--anchor" to={link.url}>
          <span dangerouslySetInnerHTML={{ __html: link.text }}></span>
        </AnchorLink>
      </li>
    ))}
  </>
)

function Header({ links, isAnchor }) {
  return (
    <header className="Header">
      <div className="Header__container">
        <h1 className="Header__title">Nefrología</h1>
        <nav className="Header__nav">
          {isAnchor ? (
            <AnchorLinks links={links} />
          ) : (
            <NavLinks links={links} />
          )}
        </nav>
      </div>
    </header>
  )
}

export default Header
