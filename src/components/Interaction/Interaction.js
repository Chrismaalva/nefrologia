import React, { useState } from "react"

import "./interaction.scss"

const respuestas = [
  "Hola, buenas tardes. ¿En qué te podemos ayudar?",
  "¿Qué síntomas tiene?",
  "¿Ha tenido fiebre?",
  "Le recomendamos visitar la sección de doctores y agendar una cita",
  "Le recomendamos contestar nuestro cuestionario para un mejor diagnóstico",
  "De nada. Hasta luego!",
  "Bienvenido al chat de nefrología",
]

function Interaction() {
  const [defaultMessage, setDefaultMessage] = useState("")
  const [message, setMessage] = useState("")
  const [responses, setResponses] = useState([])
  const handleMessage = evento => {
    setMessage(evento.target.value)
  }

  const handleBotSubmit = () => {
    switch (message.toLowerCase()) {
      case "hola":
        setResponses(state => [...state, message, respuestas[0]])
        break
      case "me siento mal":
        setResponses(state => [...state, message, respuestas[1]])
        break
      case "me duele la cabeza":
        setResponses(state => [...state, message, respuestas[2]])
        break
      case "si":
        setResponses(state => [...state, message, respuestas[3]])
        break
      default:
        setResponses([])
        setDefaultMessage(
          "Lo siento, no puedo entender lo que me estas diciendo."
        )
    }
  }

  return (
    <section className="Interaction">
      <div className="Interaction__Container">
        <h1 className="Copy__Title">Chat</h1>

        <div className="Interaction__Chat">
          {defaultMessage && <p>{defaultMessage}</p>}
          {responses.length > 0 &&
            responses.map((r, index) => (
              <p className="Interaction__Chat-UserMessage">{r}</p>
            ))}
        </div>

        <div className="Interaction__Controls">
          <input
            className="Interaction__ChatMessage"
            type="text"
            name="mensaje"
            placeholder="Ingrese su mensaje"
            onChange={handleMessage}
          />
          <button className="Interaction__ChatButton" onClick={handleBotSubmit}>
            Enviar
          </button>
        </div>
      </div>
    </section>
  )
}

export default Interaction
