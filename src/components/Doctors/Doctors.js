import React from "react"
import AlejandroRojas from "../../static/images/doctors/Alejandro-Rojas.jpg"
import CarlosNorman from "../../static/images/doctors/Carlos-Norman.jpg"
import CatalinaDuarte from "../../static/images/doctors/Catalina-Duarte.jpg"
import DanielMurillo from "../../static/images/doctors/Daniel-Murillo.jpg"
import FernandoVillanueva from "../../static/images/doctors/Fernando-Villanueva.jpg"
import MarcelinoJarquin from "../../static/images/doctors/Marcelino-Jarquin.jpg"
import MarcoSancen from "../../static/images/doctors/Marco-Sancen.jpg"
import MariaOseguera from "../../static/images/doctors/Maria-Oseguera.jpg"
import RodolfoOrtiz from "../../static/images/doctors/Rodolfo-Ortiz.jpg"

import "./doctors.scss"

function Doctors() {
  return (
    <section className="Doctors">
      <div className="Doctors__FirstRow">
        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={AlejandroRojas}
              alt="Alejandro Rojas"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Alejandro Rojas Montaño</h5>
          <p className="Copy__body">Celular: 55 5652 6987</p>
          <p className="Copy__body">
            Camino a Santa Teresa 1055 (Heroes De Padierna) 10700 Ciudad de
            México, Distrito Federal DF.
          </p>
        </div>

        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={MarcoSancen}
              alt="Marco Sancen"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Marco Sancen Martínez</h5>
          <p className="Copy__body">Celular: 55 5623 6363</p>
          <p className="Copy__body">
            Dirección: Av. Río Churubusco 601 03339 Xoco Ciudad de México.
          </p>
        </div>

        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={RodolfoOrtiz}
              alt="Rodolfo Ortiz"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Rodolfo Alfredo Ortiz Torres</h5>
          <p className="Copy__body">Celular: 55 1674 2000</p>
          <p className="Copy__body">
            Dirección: Avenida Benito Juarez 1210 (antes carretera México San
            Luis Potosí) (Valle Dorado). 78399 San Luis Potosi.
          </p>
        </div>
      </div>
      <div className="Doctors__FirstRow">
        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={FernandoVillanueva}
              alt="Fernando Villanueva"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Fernando Villanueva Martínez</h5>
          <p className="Copy__body">Celular: 55 5241 1700</p>
          <p className="Copy__body">
            Dirección: Av. Chapultepec #489 (Juárez) 06600 Ciudad de México,
            Distrito Federal DF.
          </p>
        </div>

        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={MarcelinoJarquin}
              alt="Marcelino Jarquín"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Marcelino Jarquín Vásquez</h5>
          <p className="Copy__body">Celular: 55 5265 1900</p>
          <p className="Copy__body">
            Dirección:Tlacotalpan 59 (Roma Sur) 06760 Ciudad de México, Distrito
            Federal DF.
          </p>
        </div>

        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={CarlosNorman}
              alt="Carlos Norman"
            />
          </figure>
          <h5 className="Doctor__Name">
            Dr. Carlos Norman Velázquez Gutiérrez
          </h5>
          <p className="Copy__body">Celular: 55 2848 8279</p>
          <p className="Copy__body">
            Camino a Santa Teresa 1055, Jardines del Pedregal 10700 Ciudad de
            México, Distrito Federal DF.
          </p>
        </div>
      </div>
      <div className="Doctors__FirstRow">
        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img className="Doctor__Image" src={CatalinaDuarte} alt="Anónimo" />
          </figure>
          <h5 className="Doctor__Name">Dra. Catalina Duarte Molina</h5>
          <p className="Copy__body">Celular: 55 4392 1014</p>
          <p className="Copy__body">
            Dirección: Temístocles 210, Int. 103 (Polanco) 03339 Ciudad de
            México, Distrito Federal DF.
          </p>
        </div>
        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={DanielMurillo}
              alt="Daniel Murillo"
            />
          </figure>
          <h5 className="Doctor__Name">Dr. Daniel Murillo Brambila</h5>
          <p className="Copy__body">Celular: 33 3130 2950</p>
          <p className="Copy__body">
            Dirección: Efrain Gonzalez Luna #2531 (Arcos Vallarta) 44130
            Guadalajara, Jalisco.
          </p>
        </div>
        <div className="Doctor__Details">
          <figure className="Doctor__ImageWrapper">
            <img
              className="Doctor__Image"
              src={MariaOseguera}
              alt="Maria Oseguera"
            />
          </figure>
          <h5 className="Doctor__Name">
            Dra. María Concepción Oseguera Vizcaíno
          </h5>
          <p className="Copy__body">Celular: 33 3615 3541</p>
          <p className="Copy__body">
            Dirección: ELITE GRUPO MEDICO Justo Sierra 2372 (Ladrón de Guevara)
            44600 Guadalajara, Jalisco.
          </p>
        </div>
      </div>
    </section>
  )
}

export default Doctors
