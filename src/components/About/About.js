import React from "react"
import Nefrologia from "../../static/images/about-img.jpg"

import "./about.scss"

function About() {
  return (
    <>
      <section className="About">
        <div className="About__container">
          <figure className="Header__logoWrapper" to="/">
            <img className="Header__logo" src={Nefrologia} alt="Nefrologia" />
          </figure>
          <h2 className="About__subtitle Copy__h2">¿Qué es la nefrología?</h2>
          <p className="About__body Copy__body">
            La nefrología es la especialidad médica rama de la medicina interna
            que se ocupa del estudio de la estructura y la función renal, tanto
            en la salud como en la enfermedad, incluyendo la prevención y
            tratamiento de las enfermedades renales. La palabra nefrología
            deriva de la voz griega νεφρός (nephrós), que significa riñón, y del
            sufijo -logía (estudio, tratado).
          </p>
          <p className="About__body Copy__body">
            La Nefrología puede ser definida como la especialidad clínica que se
            ocupa del estudio de la anatomía, fisiología, patología, promoción
            de salud, prevención, clínica, terapéutica y rehabilitación de las
            enfermedades del aparato urinario en su totalidad, incluyendo las
            vías urinarias que repercuten sobre el parénquima renal. A
            diferencia de la urología, esta no es una especialidad quirúrgica.
          </p>
          <p className="About__body Copy__body">
            El médico especialista en nefrología se llama nefrólogo. La
            nefrología no debe confundirse con la urología, que es la
            especialidad quirúrgica del aparato urinario y el aparato genital
            masculino.
          </p>
        </div>
        <h3 className="About__Synthoms Copy__h3">Síntomas:</h3>
        <div className="About__SynthomsContainer">
          <div className="About__SynthomDetails">
            <h5 className="About__SynthomDetails-title">
              Cambios en la micción
            </h5>
            <p className="About__SynthomDetails-description Copy__body">
              Como, por ejemplo, levantarse durante la noche a orinar (nocturia)
              o hacerlo con más frecuencia y en mayor o menor cantidad de lo
              normal.
            </p>
          </div>

          <div className="About__SynthomDetails About__SynthomDetails--middle">
            <h5 className="About__SynthomDetails-title">
              Cambios en el aspecto de la orina
            </h5>
            <p className="About__SynthomDetails-description Copy__body">
              Como un color más claro o la presencia de sangre
            </p>
          </div>

          <div className="About__SynthomDetails npr">
            <h5 className="About__SynthomDetails-title">
              Retención de líquidos
            </h5>
            <p className="About__SynthomDetails-description Copy__body">
              Lo que da lugar a una mayor hinchazón de las piernas, los
              tobillos, los pies, la cara y las manos.
            </p>
          </div>
        </div>
      </section>
    </>
  )
}

export default About
