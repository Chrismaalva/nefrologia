import React from "react"
import { Header, Footer } from "../"

function Layout({ children, headerLinks, hasLogo }) {
  return (
    <>
      <Header links={headerLinks} hasLogo={hasLogo} />
      <main>{children}</main>
      <Footer />
    </>
  )
}

export default Layout
