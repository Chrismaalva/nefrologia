import React from "react"

import EnfermedadRenal from "../../static/images/simbolo_nefi.png"
import CancerRenal from "../../static/images/image15.jpg"
import EtapasCancerRenal from "../../static/images/kidney-stages.jpg"
import EnfermedadUno from "../../static/images/ablation.jpg"

import "./information.scss"

function Information() {
  return (
    <>
      <section className="Information">
        <div className="Information__Container">
          <h1 className="Information__Title Copy__Title">Enfermedades</h1>
          <div className="Information__Details Information__Details--displayFlex">
            <figure className="Information__ImageWrapper">
              <img
                className="Information__Image"
                src={EnfermedadRenal}
                alt="Enfermedad Renal"
              />
            </figure>
            <div className="Information__Details-body">
              <h2 className="Information__Details-title">
                Insuficencia Renal Crónica (IRC)
              </h2>
              <p className="Information__Details-description Copy__body">
                Esta se caracteriza por el deterioro progresivo de la función
                renal. Muchas de las personas que portan con la enfermedad
                nisiquiera presentan sintoma alguno hasta que la enfermedad es
                ya muy avanzada lamentablemente.
              </p>
              <p className="Information__Details-description Copy__body">
                La única forma de de saber si se porta con esta enfermedad, esta
                la prueba de sangre y de orina.
              </p>
              <p className="Information__Details-description Copy__body">
                Algunas de las medidas que se deben de tomar para no tener en
                absoluto esta enfermedad, serían las siguientes:
              </p>
              <ul className="Information__List">
                <li className="Information__ListItem">
                  Ingerir alimentos que contengan poca sal
                </li>
                <li className="Information__ListItem">
                  Mantener los niveles de azucar bajo en la sangre
                </li>
                <li className="Information__ListItem">
                  Tener un peso estable; con respecto a su estatura
                </li>
                <li className="Information__ListItem">
                  Límitarse, o abstennerse de beber o fumar
                </li>
                <li className="Information__ListItem">
                  Ingerir alimentos que sean bajos en potasio; o no consumirlos
                  en exceso
                </li>
                <li className="Information__ListItem">
                  Ingiera alimentos bajos en grasas y que sean beneficos para el
                  corazón.(Futas, verduras, granos integrales,etc.)
                </li>
              </ul>
            </div>
          </div>
          <div className="Information__Details">
            <h2 className="subtitle">Insuficencia Renal Aguda (IRA)</h2>
            <p className="Information__Details-description Copy__body">
              Esta a comparación de la IRC; el deterioro de la función renal
              ocurre en una cuestión drámatica de horas. Esta también es llamada
              "Falla renal aguda" ó "lesión renal".
            </p>
            <p className="Information__Details-description Copy__body">
              Al igual que la IRC, no provoca sintoma alguno; y solo es
              detectable por medio de un análisis de laboratorio realizado
              muchas veces por motivos externos.
            </p>
            <p className="Information__Details-description Copy__body">
              Esto generalmente ocure en personas que están ya hospitalizadas, y
              aquellas que necesitan cuidados intensivos.
            </p>
            <p className="Information__Details-description Copy__body">
              Esta puede ser mortal y requiere necesariamente tratamiento
              intensivo; pero puede ser reversible; y recuperar una función
              renal (o casí) renal.
            </p>
            <table className="Information__Table">
              <thead className="Information__Table-head">
                <tr className="Information__Table-row">
                  <th className="Information__Table-th">Causas</th>
                  <th className="Information__Table-th">Sintomas</th>
                </tr>
              </thead>
              <tbody className="Information__Table-body">
                <tr className="Information__Table-th">
                  <td className="Information__Table-td">
                    Trastornos que causan coagulación dentro de los vasos
                    sanguíneos del riñón
                  </td>
                  <td className="Information__Table-td">
                    Dolor o presión en el pecho
                  </td>
                </tr>
                <tr className="Information__Table-th">
                  <td className="Information__Table-td">
                    Drogas ilegales como cocaína y heroína
                  </td>
                  <td className="Information__Table-td">
                    Falta de aire, confusión, fátiga, somnolencia, náuseas, etc.
                  </td>
                </tr>
                <tr className="Information__Table-th">
                  <td className="Information__Table-td">
                    Trastornos que causan coagulación dentro de los vasos
                    sanguíneos del riñón
                  </td>
                  <td className="Information__Table-td">
                    Convulsiones o coma en los casos graves
                  </td>
                </tr>
                <tr className="Information__Table-th">
                  <td className="Information__Table-td">
                    Ciertos antibióticos y medicamentos para la presión
                    arterial, medios de contraste intravenosos, y algunos
                    fármacos para el cáncer y el VIH.
                  </td>
                  <td className="Information__Table-td">
                    Retención de líquidos, lo que causa hinchazón en las
                    piernas, los tobillos o los pies
                  </td>
                </tr>
                <tr className="Information__Table-th">
                  <td className="Information__Table-td">
                    Coágulo de sangre por el colesterol (émbolo por colesterol)
                  </td>
                  <td className="Information__Table-td">
                    Producción reducida de orina, aunque en algunos casos la
                    producción de orina es normal
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="Information__Details">
            <h2 className="Information__Details-subtitle">Cáncer de Riñon</h2>
            <p className="Information__Details-description Copy__body">
              En los adultos, el carcinoma de células renales es el tipo más
              frecuente de cáncer de riñón y representa aproximadamente el 90
              por ciento de los tumores cancerosos. También pueden presentarse
              otros tipos menos frecuentes de cáncer de riñón. Los niños
              pequeños son más propensos a desarrollar un tipo de cáncer de
              riñón llamado <strong>tumor de Wilms</strong>.
            </p>
            <p className="Information__Details-description Copy__body">
              La incidencia del cáncer de riñón parece ser cada vez mayor. Un
              motivo puede ser el hecho de que las técnicas de diagnóstico por
              imágenes, como las tomografías computarizadas, se usan con más
              frecuencia. Estas pruebas pueden hacer que se descubran más tipos
              de cáncer renal accidentalmente. En muchos casos, el cáncer renal
              se detecta en una etapa temprana, cuando los tumores son pequeños
              y están limitados al riñón, con lo cual son más fáciles de tratar.
            </p>
          </div>
          <div className="Information__Details Information__Details--displayFlex">
            <figure className="Information__ImageWrapper">
              <img
                className="Information__Image"
                src={CancerRenal}
                alt="image15"
              />
            </figure>
            <div className="Information__Details-body">
              <h2 className="Information__Details-subtitle">
                Síntomas de cáncer de riñon
              </h2>
              <p className="Information__Details-description Copy__body">
                Los cánceres de riñón en etapas iniciales por lo general no
                causan ningún signo o síntoma,pero cuando la enfermedad es de
                mas gravedad, se podrian presentar algunos de los siguientes
                síntomas:
              </p>
              <ul className="Information__List">
                <li className="Information__ListItem">
                  Sangre en la orina (hematuria)
                </li>
                <li className="Information__ListItem">
                  Dolor en un lado de la espalda baja (no causado por una
                  lesión)
                </li>
                <li className="Information__ListItem">Cansancio (fatiga)</li>
                <li className="Information__ListItem">
                  Pérdida de peso sin explicación alguna
                </li>
                <li className="Information__ListItem">
                  Fiebre cuya causa no es por alguna infección; y que no puede
                  ser erradicada
                </li>
              </ul>
              <p className="Information__Details-description Copy__body">
                Estos signos y síntomas pueden ser causados por el cáncer de
                riñón, pero con más frecuencia se deben a otras enfermedades
                benignas. Por ejemplo, la presencia de sangre en la orina puede
                ser causada por una infección de la vejiga o del tracto urinario
                o por cálculos renales.
              </p>
            </div>
          </div>
          <div className="Information__Details">
            <h2 className="Information__Details-subtitle">
              Etapas para el cancer de Riñon
            </h2>
            <figure className="Information__ImageWrapper Information__ImageWrapper--widthInitial">
              <img
                className="Information__Image"
                src={EtapasCancerRenal}
                alt="Etapas"
              />
            </figure>

            <h3 className="subtitle">Etapa IV</h3>
            <p className="Information__Details-description Copy__body">
              Esta etapa significa que el cáncer ha crecido fuera del riñón o se
              ha propagado a otras partes del cuerpo, como a los ganglios
              linfáticos distantes o a otros órganos.
            </p>
            <p className="Information__Details-description Copy__body">
              El tratamiento para el cáncer de riñón en etapa IV depende de la
              extensión del cáncer y el estado general de la salud de la
              persona. En algunos casos, la cirugía puede aún ser parte del
              tratamiento.
            </p>
            <p className="Information__Details-description Copy__body">
              En los pocos casos donde el tumor principal parece ser extirpable
              y el cáncer sólo se ha propagado a otra área, la cirugía para
              extirpar tanto el riñón como su propagación puede ser una opción
              si la salud de la persona es bastante buena. De otro modo, el
              tratamiento con una de las terapias dirigidas generalmente es la
              primera opción.
            </p>
            <p className="Information__Details-description Copy__body">
              Si el tumor principal se puede extirpar, pero el cáncer se ha
              propagado ampliamente a otro lugar, la extirpación del riñón aún
              podría ser útil. A esto probablemente le seguirá la terapia
              sistémica, lo que podría consistir en una de las terapias
              dirigidas o inmunoterapia. No está claro si es que alguna de las
              terapias dirigidas o alguna secuencia particular es mejor que la
              otra, aunque el temsirolimus parece ser el más útil en las
              personas con cánceres de riñón que tienen un pronóstico más
              desfavorable.
            </p>
            <p className="Information__Details-description Copy__body">
              Para algunas personas, los tratamientos paliativos como la
              embolización o la radioterapia podrían ser la mejor opción. Una
              forma especial de radioterapia, llamada radiocirugía
              estereotáctica puede ser muy eficaz para tratar una metástasis
              única del cerebro. La cirugía o la radioterapia también pueden ser
              usadas para ayudar a reducir el dolor u otros síntomas de las
              metástasis en algunos otros lugares, tales como los huesos.
            </p>
            <p className="Information__Details-description Copy__body">
              Tener su dolor controlado puede ayudarle a mantener su calidad de
              vida. Los medicamentos para aliviar el dolor no interfieren con
              otros tratamientos, y el control del dolor con frecuencia le
              ayudará a estar más activo y a continuar con sus actividades
              diarias.
            </p>

            <h2 className="subtitle">Tratamientos para el cáncer de riñón.</h2>
            <p className="Information__Details-description Copy__body">
              El tratamiento depende de la edad, el estado general de salud y de
              cuán avanzado esté el cáncer. Podría incluir cirugía,
              quimioterapia o radiación, terapia biológica o dirigida. La
              terapia biológica estimula la capacidad del cuerpo para combatir
              el cáncer. La terapia dirigida es un tipo de tratamiento en el que
              se utilizan sustancias para identificar y atacar células
              cancerosas específicas sin dañar las células normales.
            </p>
            <p className="Information__Details-description Copy__body">
              El tipo de tratmiento que se utilizará, dependerá de la etapa en
              la cual se encuentre el cáncer; así como el estado de salud en
              general.
            </p>
            <p className="Information__Details-description Copy__body">
              Algunos de estos tratamientos no convencionales; serían los
              siguientes:
            </p>

            <div className="Information__Details Information__Details--displayFlex">
              <figure className="Information__ImageWrapper">
                <img
                  className="Information__Image Information__Image--width"
                  src={EnfermedadUno}
                  alt="Enfermedad 1"
                />
              </figure>
              <div className="Information__Details-body">
                <h2 className="subtitle">Crioterapia (crioablación)</h2>
                <p className="Information__Details-description Copy__body">
                  Este método utiliza frío extremo para destruir el tumor. Una
                  sonda hueca (aguja) se inserta en el tumor a través de la piel
                  o durante la laparoscopia . Se introduce gas muy frío a través
                  de la sonda, lo cual crea una bola de hielo en su extremo que
                  destruye al tumor. Para asegurarse que se destruya el tumor
                  sin causar demasiado daño a los tejidos adyacentes, el médico
                  observa cuidadosamente las imágenes del tumor durante el
                  procedimiento (con ecografía) o mide la temperatura del
                  tejido.
                </p>
                <p className="Information__Details-description Copy__body">
                  El tipo de anestesia utilizada para la crioterapia depende de
                  cómo se vaya a hacer el procedimiento. Los posibles efectos
                  secundarios incluyen sangrado y daño a los riñones o a otros
                  órganos cercanos.
                </p>
              </div>
            </div>

            <h2 className="subtitle">Ablación por radiofrecuencia.</h2>
            <p className="Information__Details-description Copy__body">
              En esta técnica, se utilizan ondas radiales de alta energía para
              calentar el tumor. Una sonda delgada, parecida a una aguja, se
              coloca a través de la piel y se mueve hasta que la punta llegue al
              tumor. Se utiliza la tomografía computarizada o la ecografía para
              guiar la colocación de la sonda. Una vez que llega al tumor, se
              pasa corriente eléctrica a través del extremo de la sonda, lo que
              calienta el tumor y destruye las células cancerosas.
            </p>
            <p className="Information__Details-description Copy__body">
              La ablación por radiofrecuencia generalmente se emplea como
              procedimiento ambulatorio, usando anestesia local (medicamento que
              adormece) en el área donde se inserta la sonda. También es posible
              que se le administre un medicamento para ayudarle a relajarse.
            </p>
            <p className="Information__Details-description Copy__body">
              No son comunes las complicaciones graves, pero pueden darse e
              incluyen sangrado y daños a los riñones o a otros órganos
              cercanos.
            </p>
            <h2 className="subtitle">Embolización arterial</h2>
            <p className="Information__Details-description Copy__body">
              Esta técnica se usa para bloquear la arteria que alimenta al riñón
              que tiene el tumor. Se coloca un catéterpequeño en una arteria de
              la parte interna del muslo y se empuja hasta que llegue a la
              arteria renal que va desde la aorta al riñón. Posteriormente, se
              inyecta un material en la arteria para bloquearla, cortando el
              suministro sanguíneo del riñón. Esto causará que el riñón (y el
              tumor en éste) mueran.
            </p>
            <p className="Information__Details-description Copy__body">
              Aunque este procedimiento no se realiza con mucha frecuencia,
              algunas veces se lleva a cabo antes de una nefrectomía radical
              para reducir el sangrado durante la operación o en pacientes que
              presentan hemorragia persistente del tumor.
            </p>
            <h2 className="title">Tipos de Cáncer</h2>
            <h3 className="subtitle">Cáncer recurrente.</h3>
            <p className="Information__Details-description Copy__body">
              Al cáncer se le llama recurrente cuando reaparece después del
              tratamiento. La recurrencia puede ser local (cerca del área del
              tumor inicial) o puede estar en órganos distantes. El tratamiento
              del cáncer de riñón que regresa después del tratamiento inicial
              depende del lugar donde recurre y los tratamientos que se han
              usado, así como de la salud de la persona y si ésta desea
              someterse a más tratamiento.
            </p>
            <p className="Information__Details-description Copy__body">
              Para los cánceres que recurren después de la cirugía inicial, la
              cirugía adicional pudiera ser una opción. De otro modo,
              probablemente se recomiende el tratamiento con terapias dirigidas
              o la inmunoterapia. Otra opción son los estudios clínicos de
              nuevos tratamientos.
            </p>
            <h3 className="subtitle">Recurrencia a distancia</h3>
            <p className="Information__Details-description Copy__body">
              El cáncer de riñón que recurre en partes distantes del cuerpo se
              trata de forma similar al cáncer en etapa IV. Sus opciones
              dependen de cuáles medicamentos de quimioterapia haya recibido (si
              así fuera) antes de que el cáncer regresara y cuánto tiempo hace
              que los recibió, así como del estado de su salud.
            </p>
            <p className="Information__Details-description Copy__body">
              Para los cánceres que progresan (continúan creciendo o
              propagándose) durante el tratamiento con terapia dirigida o
              terapia con citocinas, emplear otro tipo de terapia dirigida o
              inmunoterapia puede que sea útil. Si éstos no surten efecto, se
              puede tratar quimioterapia, especialmente en las personas con
              cáncer de células renales no claras. Los cánceres recurrentes a
              veces pueden ser difíciles de tratar.
            </p>
            <p className="Information__Details-description Copy__body">
              Para algunas personas con cáncer de riñón recurrente, los
              tratamientos paliativos como la embolización o la radioterapia
              podrían ser la mejor opción. El control de los síntomas, como el
              dolor, es una parte importante del tratamiento en cualquier etapa
              de la enfermedad.
            </p>
          </div>
        </div>
        <div className="Information__Details">
          <h3 className="Information__Details-subtitle">
            Enfermedades renales en la ciudad de San Luis Potosí
          </h3>
          <p className="Information__Details-description Copy__body">
            Ahora se verán diferentes noticias relacionadas a las enfermedades
            renales. Ubicando esto, en el estado de San Luis Potosí, México.
          </p>
          <p className="Information__Details-description Copy__body">
            El Jefe de Prestaciones Médicas del Seguro Social, doctor Francisco
            Javier Ortiz Nesme dio a conocer que los pacientes con falla renal
            en seguimiento con el Instituto se incrementan año con año en forma
            importante, por lo que el reto es que el paciente esté en control de
            su diabetes, ya que es el principal motivo de incremento.
          </p>
          <p className="Information__Details-description Copy__body">
            Ortiz Nesme dio a conocer que el Instituto a nivel estatal maneja
            más de 1,500 pacientes en seguimiento en hemodiálisis y diálisis
            peritonial, que son las dos formas de atender a pacientes con falla
            renal.
          </p>
          <p className="Information__Details-description Copy__body">
            El Jefe de prestaciones médicas del IMSS exhortó a la población
            diabética a que reconozcan su enfermedad porque en eso radican los
            siguientes pasos a seguir: “nos encontramos a nivel de medicina
            familiar pacientes que no se hacen caso, no llevan dieta ni
            tratamiento, así a largo plazo se complica”, detalló.
          </p>
          <p className="Information__Details-description Copy__body">
            Explicó que las primeras complicaciones de una persona diabética
            comenzarán a los 10 años de haberse iniciado este padecimiento,
            empezarán los problemas de la vista, cataratas, extremidades y hasta
            amputaciones y finalmente llegará a afectar el riñón.
          </p>
          <p className="Information__Details-description Copy__body">
            A continuación se presentará un enlace a un documento pdf en el cual
            se podrá ver más a fondo la estádistica relacionada a la Nefrología
            dentro del estado
          </p>
          <a
            className="Information__Details-link"
            href="https://martin-de-rivia.000webhostapp.com/Proyecto/EST2017.pdf"
          >
            Estadística Nefrología en SLP
          </a>
        </div>
      </section>
    </>
  )
}

export default Information
