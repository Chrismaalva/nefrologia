import React from "react"

import "./faq.scss"
function Faq() {
  return (
    <>
      <div className="Faq__Container">
        <section class="preguntas">
          <h1 class="title">Preguntas Frecuentes</h1>

          <div class="preguntas-topic">
            <h3 class="preguntas-subtitle">Enfermedad Renal</h3>
            <h4 class="preguntas-question">
              ¿Qué es la enfermedad renal crónica?
            </h4>
            <p class="preguntas-answer">
              Enfermedad renal crónica (ERC) es un término que se utiliza para
              describir el daño renal o la función renal reducida
              (independientemente de la causa) que persiste durante más de 3
              meses. En ocasiones, la ERC produce insuficiencia renal o
              insuficiencia renal terminal (IRT), que requiere diálisis o un
              trasplante de riñón para mantenerse con vida.
            </p>

            <h4 class="preguntas-question">
              ¿Qué es la insuficiencia renal terminal?
            </h4>

            <p class="preguntas-answer">
              {" "}
              La insuficiencia renal terminal (IRT) se produce cuando existe una
              falla renal total y permanente. Cuando el riñón deja de funcionar,
              el cuerpo retiene líquido y se acumulan desechos nocivos. Si usted
              tiene IRT, necesitará diálisis o un trasplante de riñón para
              mantenerse con vida.
            </p>

            <h4 class="preguntas-question">
              ¿Existe cura para la ERC o la IRT?
            </h4>

            <p class="preguntas-answer">
              No existe cura para la ERC o la IRT. Sin embargo, existen maneras
              de abordar la ERC y la IRT que pueden ayudar a aquellas personas
              que tienen estas enfermedades a vivir una vida plena y productiva.
              Si usted tiene ERC, es importante que se realice controles
              regularmente. Con diagnóstico precoz y tratamiento, se puede
              retardar el avance de la enfermedad.
            </p>
          </div>

          <div class="preguntas-topic">
            <h3 class="preguntas-subtitle">Tratamientos de diálisis</h3>
            <h4 class="preguntas-question">¿Qué es la diálisis?</h4>
            <div class="preguntas-answer">
              <p>
                En palabras simples, la diálisis es una manera de limpiar su
                sangre. Es un tratamiento médico para personas con IRT a través
                del cual se logran las funciones que los riñones sanos
                desarrollan, que incluyen:
                <ul class="preguntas-list">
                  <li>
                    La eliminación de desechos, sal y exceso de agua en su
                    cuerpo
                  </li>
                  <li>La regulación del equilibrio del líquido corporal </li>
                  <li>El control de la presión arterial</li>
                </ul>
              </p>
            </div>

            <h4 class="preguntas-question">
              ¿Cuáles son los distintos tipos de tratamientos de diálisis para
              la enfermedad renal?
            </h4>
            <p class="preguntas-answer">
              {" "}
              Hay 2 tipos principales de diálisis: la hemodiálisis (HD) y la
              diálisis peritoneal (DP), y cada una tiene diferentes ventajas,
              requisitos y opciones de horarios. En función de su estado de
              salud, su estilo de vida y otros factores, usted y su médico
              deberán decidir qué tipo de diálisis resulta adecuada para usted.
            </p>
            <h4 class="preguntas-question">
              {" "}
              <strong>Hemodiálisis</strong>
            </h4>
            <p class="preguntas-answer">
              El prefijo hemo significa “sangre” y diálisis significa “filtro”.
              En pocas palabras, la hemodiálisis es un procedimiento por el cual
              se filtra la sangre fuera del cuerpo mediante una máquina que se
              denomina dializador o “riñón artificial”, y luego, se la reingresa
              al cuerpo.
            </p>
          </div>

          <div class="preguntas-topic">
            <h3 class="preguntas-subtitle">
              Viajar con el tratamiento de la dialisis
            </h3>
            <h4 class="preguntas-question">
              Si recibo diálisis y estoy planeando viajar, ¿qué debo hacer?
            </h4>
            <p class="preguntas-answer">
              Usted debe asegurarse de que tendrá los medicamentos de diálisis
              apropiados y acceso a un centro de diálisis.
            </p>
          </div>

          <div class="preguntas-topic">
            <h3 class="preguntas-subtitle">Dietas para ERC y la IRT</h3>
            <h4 class="preguntas-question">
              ¿Existe una dieta especial para la ERC y la IRT?
            </h4>
            <div class="preguntas-answer">
              <p>
                Lo que deba comer, y cuánto, dependerá de la etapa de la ERC en
                la que se encuentre y de cuán bien funcionen sus riñones.
                Consulte a su médico o dietista renal para conocer los
                requisitos alimentarios específicos para su afección renal.
              </p>
              <p>Sin embargo, en general, usted deberá:</p>
              <ul class="preguntas-list">
                <li>
                  Disminuir el consumo de proteínas, especialmente si está en la
                  etapa 3 o 4 de la ERC.
                </li>
                <li>
                  Prescindir de la sal: el sodio puede provocar un efecto
                  negativo para los riñones y aumentar la presión arterial.
                </li>
                <li>
                  Cuidarse del exceso de fósforo: es un mineral que se encuentra
                  en muchos alimentos y ayuda a controlar la frecuencia
                  cardíaca. Si tiene ERC, el exceso de fósforo puede acumularse
                  en los riñones.
                </li>
              </ul>
            </div>
          </div>

          <div class="preguntas-topic">
            <h3 class="preguntas-subtitle">Vivir con diálisis</h3>
            <h4 class="preguntas-question">
              Ahora que recibí el diagnóstico de IRT, estoy atravesando
              dificultades emocionales. ¿Qué puedo hacer?
            </h4>
            <p class="preguntas-answer">
              Recibir la noticia de que tiene IRT puede desatar un sinfín de
              emociones. Es una enfermedad grave, pero gracias a los
              tratamientos de diálisis renal y otros tipos de soporte, aún podrá
              mantenerse con vida e incluso vivir plenamente. Si le resulta
              difícil mantenerse optimista, hable con un amigo de confianza o un
              ser querido que lo pueda ayudar. Si necesita un asesor
              profesional, hable con un trabajador social que podrá ayudarlo a
              superar cualquier dificultad emocional y también lo conectará con
              otros asesores o recursos.{" "}
            </p>
          </div>

          <div class="preguntas-topic">
            <h4 class="preguntas-question">
              ¿Cómo afectará la diálisis mi vida sexual?
            </h4>
            <p class="preguntas-answer">
              Probablemente se sentirá muy cansado y puede que tenga
              sentimientos de depresión fluctuantes. Pero es importante saber
              que, si bien la diálisis puede afectar su vida sexual, no
              significa que le pondrá fin.
            </p>
          </div>

          <div class="preguntas-topic">
            <h4 class="preguntas-question">
              ¿Cómo puedo pedir ayuda a otras personas?
            </h4>
            <div class="preguntas-answer">
              <p>
                Recurrir a otros en busca de ayuda no es poca cosa, sobre todo
                si se jacta de ser independiente.
              </p>
              <p>
                Si bien aún hay muchas cosas que podrá hacer por sí solo,
                necesitará afrontar las realidades físicas y emocionales de
                someterse a un tratamiento de diálisis. Para empezar, usted
                puede:
              </p>
              <ul class="preguntas-list">
                <li>
                  Hablar con un trabajador social: le brindará orientación sobre
                  todas las maneras en que puede conseguir apoyo
                </li>
                <li>
                  Hablar con amigos y familiares: tenga una conversación con sus
                  seres queridos y considere el poder de los medios sociales
                  para difundir información entre grupos más grandes de amigos y
                  conocidos
                </li>
                <li>
                  Formar y unirse a grupos de apoyo: consulte al trabajador
                  social para obtener consejos sobre cómo encontrar o formar un
                  grupo de apoyo
                </li>
              </ul>
            </div>
          </div>
        </section>
      </div>
    </>
  )
}

export default Faq
