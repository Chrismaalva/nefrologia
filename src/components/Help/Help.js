import React from "react"
import { Link } from "gatsby"

import { ReactComponent as ImagenAyudaUno } from "../../static/logos/ayuda1.svg"
import { ReactComponent as ImagenAyudaDos } from "../../static/logos/ayuda2.svg"
import "./help.scss"
function Help() {
  return (
    <>
      <div className="Help__Container">
        <section className="ayuda">
          <h1 className="Copy__Title">Ayuda</h1>
          <div className="ayuda-container">
            <div className="ayuda-box">
              <ImagenAyudaUno className="Help__Image" />
              <p className="ayuda-description">
                Si usted es paciente que ya tiene un historial y desea conocer a
                fondo soluciones para su padecimiento, lo invitamos a que llene
                nuestro formulario con sencillas preguntas que nos facilitarán
                ayudarle a diagnosticar la severidad de su enfermedad.
              </p>
              <Link target="_blank" to="/cuestionario" rel="noreferrer">
                Llenar cuestionario
              </Link>
            </div>
            <div className="ayuda-box">
              <ImagenAyudaDos className="Help__Image" />
              <p className="ayuda-description">
                Le recomendamos que si tiene alguna duda referente a nefrología
                y sus síntomas puede consultar nuestra sección de preguntas
                frecuentes antes de contactar al equipo médico.
              </p>
              <Link rel="noreferrer" target="_blank" to="/preguntas-frecuentes">
                Ir a sección
              </Link>
            </div>
          </div>
        </section>
        <section className="grupos-ayuda">
          <h1 className="Copy__Title">Grupos de Ayuda</h1>
          <div className="grupos-ayuda-container">
            <ul>
              <li>
                <strong>Insuficiencia Renal</strong>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.niddk.nih.gov/health-information/informacion-de-la-salud/enfermedades-rinones/ayuda-financiera-tratamiento-insuficiencia-renal"
                >
                  National Institute of Diabetes and Digestive and Kidney
                  Diseases (En español)
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="http://www.soloporayudar.org/programas/insuficiencia-renal-cronica/"
                >
                  Sólo por ayudar
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.kidney.org/atoz/content/vivir-bien"
                >
                  National Kidney Foundation (En español)
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <strong>Diálisis</strong>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.freseniuskidneycare.com/es/thriving-on-dialysis/personal-life/building-support"
                >
                  Freesenius Kidney Care
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://asociacionale.org/acciones/programa-apoyo-para-hemodialisis/"
                >
                  Ale
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="http://www.senefro.org/modules.php?name=grupos&d_op=viewgroup&idgroup=4083&idgroupcontent=1351"
                >
                  Grupo de apoyo al desarrollo de la diálisis peritoneal en
                  España (GADDPE)
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <strong>Ayuda General</strong>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="http://www.fundrenal.org.mx/about.html"
                >
                  Fundación Mexicana del Riñon A.C.
                </a>
              </li>
              <li>
                <a target="_blank" rel="noreferrer" href="http://adaer.org/">
                  Asociación de Ayuda al Enfermo Renal (ADAER)
                </a>
              </li>
              <li>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://femetre.org.mx/"
                >
                  Federación Mexicana de Enfermos y Transplantados Renales
                </a>
              </li>
            </ul>
          </div>
        </section>
      </div>
    </>
  )
}

export default Help
