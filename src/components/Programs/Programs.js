import React from "react"
import ProgramMexico1 from "../../static/images/programs/Mprog1.jpg"
import ProgramMexico2 from "../../static/images/programs/Mprog2.jpg"
import Program1 from "../../static/images/programs/Programa1.jpg"
import Program2 from "../../static/images/programs/Programa2.jpg"

import "./programs.scss"

function Programs() {
  return (
    <section className="Programs">
      <div className="Programs__container">
        <div className="Programs__Details">
          <h3 className="Copy__Title">Programas en Mexico</h3>
          <p className="Copy__body">
            Dentro del país de México, se halla el{" "}
            <strong>Colegio de Nefrólogos</strong>; que se especializa en el
            tratado y en el cuidado de los riñones. Esta misma, organiza en
            ocasiones una serie de eventos especiales, o actividades academicas
            para dar información al público acerca de las enfermedades crónicas
            que pueden haber; oh demás información.
          </p>
          <p className="Copy__body">
            Como se puede notar, estas actividades pueden ser de distintas y muy
            diversas maneras, además de las pocas actividades que se han dado
            recientemente.
          </p>
          <div className="Programs__ImageContainer">
            <figure className="Programs__ImageWrapper">
              <img
                className="Programs__Image"
                src={ProgramMexico1}
                alt="Programa México 1"
              />
            </figure>
            <figure className="Programs__ImageWrapper">
              <img
                className="Programs__Image"
                src={ProgramMexico2}
                alt="Programa México 2"
              />
            </figure>
          </div>
        </div>
        <div className="Program-Mexico1">
          <h3 className="title">Programas a nivel Mundial</h3>
          <p className="Copy__body">
            Ahora,en cuanto al nivel mundial se refiere; podemos encontrar
            algunos programas o iniciativas empezadas por partes de la OMS.
            Dentro de esta lista no hay muchos en realidad; y desde hace 4 años
            no se han realizado mas de 3 o 2 programas.
          </p>
          <p className="Copy__body">
            A continuación se mostrararan algunos de estos que han sido
            realizados o implementados por aunque sea, un pequeño lapso de
            tiempo.
          </p>
        </div>

        <div className="Programs__Details">
          <h4 className="Copy__h4 Programs__h4Title">
            Seminario Virtual, de riñones y salud de la mujer
          </h4>
          <p className="Copy__body">
            Como lo dice en el mismo nombre; este seminario abordaba temas
            relacionados a la epidemologia, vigilancia, tratamiento y control de
            la enfermedad renal crónica; enfocada, claro, al sexo femenino.
          </p>
          <div className="Programs__Information">
            <figure className="Programs__ImageWrapper">
              <img
                className="Programs__Image"
                src={Program1}
                alt="Programa 1"
              />
            </figure>
            <div className="Programs__Information-Description">
              <p className="Copy__body">
                Algunos de los especialistas que se involucraron dentro de esta
                conferencia; fueron los siguientes:
              </p>
              <ul className="Programs__List">
                <li className="Programs__ListItem">Dr. Alonso Cueto-Manzano</li>
                <li className="Programs__ListItem">
                  Dra. Carlota Gonzales Bedat
                </li>
                <li className="Programs__ListItem">Guillermo Rosa Diez</li>
                <li className="Programs__ListItem">
                  Dr. Jose Antonio Escamilla-Cejudo
                </li>
                <li className="Programs__ListItem">
                  Dr. Juan Manuel Fernandez Cean
                </li>
              </ul>
            </div>
          </div>
          <h4 className="Copy__h4 Programs__h4Title">
            Curso Virtual de Prevención y Manejo de la Enfermedad Renal Crónica
          </h4>
          <p className="Copy__body">
            Este curso virtual; buscaba contribuiur a fortalecer las capacidades
            de los equipos de atencion primaria en los países de la Región de
            las Américas para prevenir la enfermedad reak crónica(ERC); así como
            toda la atención que debe de tener esta enfermedad.
          </p>
          <p className="Copy__body">
            Imagen publicitaria para el curso mencionado:
          </p>
          <figure className="Programa2">
            <img src={Program2} alt="Programa 2" />
          </figure>
          <p className="Copy__body">
            Este curso virtual fue desarrollada, en coordinación con la Sociedad
            Latinoamericana de Nefrología e Hipertensión(SLANH); y estuvo
            disponible en los lenguages de español y portugues.
          </p>
          <p className="Copy__body">
            El proyecto consto de 10 módulos que incluían los siguientes temas:
          </p>
          <ul className="Programs__List">
            <li className="Programs__ListItem">Módulo 0: Epidemiología.</li>
            <li className="Programs__ListItem">
              Módulo 1: Definción y factores de riesgo de la ERC.
            </li>
            <li className="Programs__ListItem">
              Módulo 2: Evaluación, diagnóstico y estratificación.
            </li>
            <li className="Programs__ListItem">
              Módulo 3: Diagnóstico y manejo de la proteinuria.
            </li>
            <li className="Programs__ListItem">
              Módulo 4: Manejo higiénico y medicamentoso del paciente
              hipertenso.
            </li>
            <li className="Programs__ListItem">
              Módulo 5: Estretegias para formentar el auto cuidado.
            </li>
            <li className="Programs__ListItem">
              Módulo 6: Manejo de las comorbilidades.
            </li>
            <li className="Programs__ListItem">
              Módulo 7: Prevención y pregresión de la ERC en el paciente
              diabético.{" "}
            </li>
            <li className="Programs__ListItem">
              Módulo 8: Referencia al nefrólogo.
            </li>
            <li className="Programs__ListItem">
              Módulo 9: ERC de las poblaciones agrícolas de Centroamérica.
            </li>
          </ul>
        </div>
      </div>
    </section>
  )
}

export default Programs
